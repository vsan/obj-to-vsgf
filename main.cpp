#include <iostream>
#include <unordered_map>
#include <limits>
#include <iomanip>

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include "HydraVSGFExport.h"
#include "LiteMath.h"

using namespace HydraLiteMath;

struct vertex
{
    float3 pos;
    float2 uv;
    float3 normal;
    float4 tangent;

    bool operator==(const vertex& other) const
    {
      return pos == other.pos && uv == other.uv &&
             normal == other.normal && tangent == other.tangent;
    }

};

namespace std {
    template<> struct hash<vertex> {
        size_t operator()(vertex const& vertex) const {
          return ((hash<float3>()(vertex.pos) ^ (hash<float3>()(vertex.normal) << 1)) >> 1) ^ (hash<float2>()(vertex.uv) << 1);
        }
    };
}

bool obj2vsgf(const std::string &input_path, const std::string &output_path)
{
  HydraGeomData data;

  std::vector <vertex> vertices;
  std::vector <uint32_t> indices;

  tinyobj::attrib_t attrib;
  std::vector<tinyobj::shape_t> shapes;
  std::vector<tinyobj::material_t> materials;
  std::string err;

  if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &err, input_path.c_str()))
  {
    std::cerr << "Error reading input file: " << err << std::endl;
    //throw std::runtime_error(err);
  }

  std::unordered_map<vertex, uint32_t> uniqueVertices = {};
  std::vector<uint32_t> mat_ids;

  bool has_normals = false;

  for (const auto& shape : shapes)
  {
    mat_ids.insert(std::end(mat_ids), std::begin(shape.mesh.material_ids), std::end(shape.mesh.material_ids));

    for (const auto& index : shape.mesh.indices)
    {
      vertex vertex = {};

      vertex.pos = {
              attrib.vertices[3 * index.vertex_index + 0],
              attrib.vertices[3 * index.vertex_index + 1],
              attrib.vertices[3 * index.vertex_index + 2]
      };

      if(index.normal_index > 0)
      {
        has_normals = true;
        vertex.normal = {
                attrib.normals[3 * index.normal_index + 0],
                attrib.normals[3 * index.normal_index + 2],
                attrib.normals[3 * index.normal_index + 2]
        };
      }

      if(index.texcoord_index > 0)
      {
        vertex.uv = {
                attrib.texcoords[2 * index.texcoord_index + 0],
                1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
        };
      }

//      vertex.color = { 1.0f, 1.0f, 1.0f };

      vertex.tangent = {0.0f, 0.0f, 0.0f, 0.0f};

      if (uniqueVertices.count(vertex) == 0)
      {
        uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
        vertices.push_back(vertex);
      }

      indices.push_back(uniqueVertices[vertex]);
    }
  }

  std::vector<float> pos;
  std::vector<float> normals;
  std::vector<float> uvs;
  std::vector<float> tangents;

  for(auto &v : vertices)
  {
    pos.push_back(v.pos.x);
    pos.push_back(v.pos.y);
    pos.push_back(v.pos.z);
    pos.push_back(1.0f);

    if(has_normals)
    {
      normals.push_back(v.normal.x);
      normals.push_back(v.normal.y);
      normals.push_back(v.normal.z);
      normals.push_back(0.0f);
    }

    uvs.push_back(v.uv.x);
    uvs.push_back(v.uv.y);

    tangents.push_back(v.tangent.x);
    tangents.push_back(v.tangent.y);
    tangents.push_back(v.tangent.z);
    tangents.push_back(v.tangent.w);
  }

  for(auto& mid : mat_ids) { // fix material id
    if(mid == uint32_t(-1)) 
      mid = 0;
  }

  data.setData(vertices.size(), pos.data(), normals.data(), tangents.data(), uvs.data(),
               indices.size(), indices.data(), mat_ids.data());

  if(!data.write(output_path, err))
  {
    std::cerr << err << std::endl;
    return false;
  }

  return true;
}

bool vsgf2vsgf_normalize(const std::string& input, const std::string& output)
{
  HydraGeomData data;
  data.read(input);
  
  if(data.sizeInBytes() == 0)
    return false;
  
  const float fltMax = std::numeric_limits<float>::max();
  float3 boxMin(fltMax,fltMax,fltMax);
  float3 boxMax(-fltMax,-fltMax,-fltMax);
  float* vertData = const_cast<float*>(data.getVertexPositionsFloat4Array());
  for(size_t i=0;i<data.getVerticesNumber();i++)
  {
    float3 vertex = float3(vertData[i*4+0], vertData[i*4+1], vertData[i*4+2]);
    boxMin.x = std::min(boxMin.x, vertex.x);
    boxMin.y = std::min(boxMin.y, vertex.y);
    boxMin.z = std::min(boxMin.z, vertex.z);
    boxMax.x = std::max(boxMax.x, vertex.x);
    boxMax.y = std::max(boxMax.y, vertex.y);
    boxMax.z = std::max(boxMax.z, vertex.z);
  }

  const float3 boxCenter  = 0.5f*(boxMin + boxMax);
  const float3 boxSizeInv = 1.0f/(boxMax - boxMin);
  
  //boxMin = float3(fltMax,fltMax,fltMax);
  //boxMax = float3(-fltMax,-fltMax,-fltMax);
  for(size_t i=0;i<data.getVerticesNumber();i++)
  {
    const float3 vO = float3(vertData[i*4+0], vertData[i*4+1], vertData[i*4+2]);
    const float3 vT = 2.0f*(vO - boxCenter)*boxSizeInv;
    vertData[i*4+0] = vT.x;
    vertData[i*4+1] = vT.y;
    vertData[i*4+2] = vT.z;
    //boxMin.x = std::min(boxMin.x, vT.x);
    //boxMin.y = std::min(boxMin.y, vT.y);
    //boxMin.z = std::min(boxMin.z, vT.z);
    //boxMax.x = std::max(boxMax.x, vT.x);
    //boxMax.y = std::max(boxMax.y, vT.y);
    //boxMax.z = std::max(boxMax.z, vT.z);
  }

  //std::cout << boxMin.x << " " << boxMin.y << " " << boxMin.z << std::endl;
  //std::cout << boxMax.x << " " << boxMax.y << " " << boxMax.z << std::endl;

  std::string err;
  if(!data.write(output.c_str(), err))
  {
    std::cerr << err << std::endl;
    return false;
  }

  return true;
}

bool vsgf2obj(const std::string& input, const std::string& output)
{
  HydraGeomData data;
  data.read(input);
  
  if(data.sizeInBytes() == 0)
    return false;

  std::ofstream fout(output.c_str());
  
  fout << "# 3ds vsgf2obj stupid exporter: v0.1" << std::endl << std::endl; 
  fout << "# " << std::endl;
  fout << "# object " << input.c_str() << std::endl;
  fout << "# " << std::endl << std::endl;

  const size_t vertNum = data.getVerticesNumber();
  const size_t triNum  = data.getIndicesNumber()/3; 

  const float* vpos4f  = data.getVertexPositionsFloat4Array();
  const float* vnorm4f = data.getVertexNormalsFloat4Array();
  const float* vtan4f  = data.getVertexTangentsFloat4Array();
  const float* vtex2f  = data.getVertexTexcoordFloat2Array();

  const uint32_t* indices = data.getTriangleVertexIndicesArray();
  const int DIJITS = 5;
  
  std::cout << std::endl;
  for(size_t i=0;i<vertNum;i++)
    fout << "v  " << std::setprecision(DIJITS) << vpos4f[i*4+0] << " " << vpos4f[i*4+1] << " " << vpos4f[i*4+2] << std::endl; 
  fout << "# " << vertNum << " vertices" << std::endl << std::endl;
  
  std::cout << std::endl;
  for(size_t i=0;i<vertNum;i++)
    fout << "vn " << std::setprecision(DIJITS) << vnorm4f[i*4+0] << " " << vnorm4f[i*4+1] << " " << vnorm4f[i*4+2] << std::endl; 
  fout << "# " << vertNum << " normals" << std::endl << std::endl;

  std::cout << std::endl;
  for(size_t i=0;i<vertNum;i++)
    fout << "vt " << std::setprecision(DIJITS) << vtex2f[i*2+0] << " " << vtex2f[i*2+1] << std::endl; 
  fout << "# " << vertNum << " texture coords" << std::endl << std::endl;
  
  for(size_t i=0;i<triNum;i++) {
    const uint32_t A = indices[i*3+0]+1;
    const uint32_t B = indices[i*3+1]+1;
    const uint32_t C = indices[i*3+2]+1;
    fout << "f " << A << "/" << A << "/" << A << " " << B << "/" << B << "/" << B << " " << C << "/" << C << "/" << C << std::endl;
  }
  fout << "# " << triNum << " polygons" << std::endl << std::endl;

  return true;
}

int main(int argc, const char** argv)
{
  if(argc != 3)
    std::cout << "Incorrect arguments. Usage: [in_obj_file_path] [out_vsgf_file_path]" << std::endl;

  std::string input(argv[1]);
  std::string output(argv[2]);

  std::cout << "input  = '" << input.c_str() << "'" << std::endl;
  std::cout << "output = '" << output.c_str() << "'" << std::endl;

  if(input.find(".obj") != std::string::npos && 
     output.find(".vsgf") != std::string::npos)
  {
    std::cout << "call 'obj2vsgf' " << std::endl;
    obj2vsgf(input, output);
  }
  else if(input.find(".vsgf") != std::string::npos && 
          output.find(".obj") != std::string::npos)
  {
    std::cout << "call 'vsgf2obj' " << std::endl;
    vsgf2obj(input, output);
  }
  else if(input.find(".vsgf") != std::string::npos && 
          output.find(".vsgf") != std::string::npos)
  {
    std::cout << "call 'vsgf2vsgf' (bbox normalization)" << std::endl;
    vsgf2vsgf_normalize(input, output);
  }

  
  return 0;
}